<html>
<head>
    <script src="https://kit.fontawesome.com/e2ac9cc532.js" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Форма с базой данных</title>
    <link  href="style.css" rel="stylesheet"  media="all"/>
</head>
<div class="form-container">
<body>
<?php
if (!empty($messages)) {
    print('<div id="messages">');
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}

?>
<div class="in-form-container">
<form action="index.php" accept-charset="UTF-8" method="POST">


    <div class="set">

        <div class="formname">
            <label>
                <?php if ($errors['fio']) {print '
<a>
        <img src="https://downloader.disk.yandex.ru/preview/e48bcadbd71390e487be9c8ec2244cc7a0700079835ac6c7d4bc0656a78af1be/60884ec9/SriIYbvsnSXOQ-PYJeV7JUkA7cC7gWvxoAhZqHrzBPfxEVbdpR6ZGaqkNnGVoH5UdzO3P-HlAdu8yj3T197U7Q%3D%3D?uid=0&filename=warn.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048" 
        alt="Предупреждение"
      </a>
';} ?>
                Имя
    <input   class="formname" type="text"  name="fio" placeholder="Введите имя"
         value="<?php print $values['fio']; ?>">
            </label>

        </div>

        <div class="form_mail">

        <label>
            <?php if ($errors['email']) {print '<a>
        <img src="https://downloader.disk.yandex.ru/preview/e48bcadbd71390e487be9c8ec2244cc7a0700079835ac6c7d4bc0656a78af1be/60884ec9/SriIYbvsnSXOQ-PYJeV7JUkA7cC7gWvxoAhZqHrzBPfxEVbdpR6ZGaqkNnGVoH5UdzO3P-HlAdu8yj3T197U7Q%3D%3D?uid=0&filename=warn.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048" 
        alt="Предупреждение"
      </a>';} ?>
            Почта</label>
    <input class="formmail" type="email" name="email" placeholder="Введите почту"  value="<?php print $values['email']; ?>">
        </div>

</div>


    <div class="set">

        <div class="birthday">
    <label >
        <?php if ($errors['year']) {print '<a>
        <img src="https://downloader.disk.yandex.ru/preview/e48bcadbd71390e487be9c8ec2244cc7a0700079835ac6c7d4bc0656a78af1be/60884ec9/SriIYbvsnSXOQ-PYJeV7JUkA7cC7gWvxoAhZqHrzBPfxEVbdpR6ZGaqkNnGVoH5UdzO3P-HlAdu8yj3T197U7Q%3D%3D?uid=0&filename=warn.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048" 
        alt="Предупреждение"
      </a>';} ?>

        Год рождения<br/>
        <input placeholder="Введите год рождения"  value="<?php print $values['year_value']; ?>" id="dr" name="birthyear"
        type="number"/>
    </label>
    </div>

        <div>
        <div class="limbs" <?php if($errors['limb']){print 'style="margin-left:53px;margin-top:4px"';}?>>
            <?php if($errors['limb']){print '<a>
        <img src="https://downloader.disk.yandex.ru/preview/e48bcadbd71390e487be9c8ec2244cc7a0700079835ac6c7d4bc0656a78af1be/60884ec9/SriIYbvsnSXOQ-PYJeV7JUkA7cC7gWvxoAhZqHrzBPfxEVbdpR6ZGaqkNnGVoH5UdzO3P-HlAdu8yj3T197U7Q%3D%3D?uid=0&filename=warn.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048" 
        alt="Предупреждение"
      </a>';}?>
        Кол-во конечностей<br/>
            <div class="radio-container">
        <input <?php if($values['limb_value']=="3"){print 'checked';}?> type="radio" id="3l" name="radio1" value="3"/>
            <label for="3l">3</label>
        <input <?php if($values['limb_value']=="4"){print 'checked';}?> type="radio" id="4l" name="radio1" value="4"/>
            <label for="4l">4</label>
        </div>
        </div>
    </div>
    </div>


    <div class="set_vertical">

        <div class="gender">
            <?php if($errors['sex']){print '<a>
        <img src="https://downloader.disk.yandex.ru/preview/e48bcadbd71390e487be9c8ec2244cc7a0700079835ac6c7d4bc0656a78af1be/60884ec9/SriIYbvsnSXOQ-PYJeV7JUkA7cC7gWvxoAhZqHrzBPfxEVbdpR6ZGaqkNnGVoH5UdzO3P-HlAdu8yj3T197U7Q%3D%3D?uid=0&filename=warn.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048"
        alt="Предупреждение"
      </a>';}?>
    Пол:

            <div class="radio-container">
            <input <?php if($values['sex_value']=="man"){print 'checked';}?> type="radio" id="male" name="radio2" value="man" />
                <label for="male">Мужской </label>

            <input <?php if($values['sex_value']=="woman"){print 'checked';}?> type="radio" id="female" name="radio2" value="woman"/>
                <label for="female">Женский</label>
            </div>


     <div class="abil">
      <label style="color:white;">
          <?php if ($errors['abil']) {print '<a>
        <img src="https://downloader.disk.yandex.ru/preview/e48bcadbd71390e487be9c8ec2244cc7a0700079835ac6c7d4bc0656a78af1be/60884ec9/SriIYbvsnSXOQ-PYJeV7JUkA7cC7gWvxoAhZqHrzBPfxEVbdpR6ZGaqkNnGVoH5UdzO3P-HlAdu8yj3T197U7Q%3D%3D?uid=0&filename=warn.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048"
        alt="Предупреждение"
      </a>';} ?>
        Cверхспособности
        <br/>
         <div >
            <select id="sp" name="select1[]" multiple="multiple">

                <option  <?php if($values['abil_value']=="god"){print 'selected';}?>value="god">Бессмертие</option>
                <option  <?php if($values['abil_value']=="wall"){print 'selected';}?>value="wall">Прохождение сквозь стены</option>
                <option  <?php if($values['abil_value']=="levity"){print 'selected';}?>value="levity">Левитация</option>
                <option  <?php if($values['abil_value']=="kos"){print 'selected';}?>value="kos">Дискретка на 5</option>
                <option  <?php if($values['abil_value']=="kol"){print 'selected';}?>value="kol">Диффуры на 5</option>

            </select>
         </div>
      </label>
     </div>
 </div>
    <div class="biogr">
        <label style="color:white;"> <?php if ($errors['bio']) {print '<a>
        <img src="https://downloader.disk.yandex.ru/preview/e48bcadbd71390e487be9c8ec2244cc7a0700079835ac6c7d4bc0656a78af1be/60884ec9/SriIYbvsnSXOQ-PYJeV7JUkA7cC7gWvxoAhZqHrzBPfxEVbdpR6ZGaqkNnGVoH5UdzO3P-HlAdu8yj3T197U7Q%3D%3D?uid=0&filename=warn.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048"
        alt="Предупреждение"
      </a>';} ?>
            Биография
        </label>
        <textarea id="biog" name="textarea1"><?php print $values['bio_value'];?></textarea>
    </div>
</div>

    <div class="set">

        <div>
            <div class="set_check">
                <div class="check_text">
            <label style="color:white;">
                <?php if ($errors['check']) {print '<a>
        <img src="https://downloader.disk.yandex.ru/preview/e48bcadbd71390e487be9c8ec2244cc7a0700079835ac6c7d4bc0656a78af1be/60884ec9/SriIYbvsnSXOQ-PYJeV7JUkA7cC7gWvxoAhZqHrzBPfxEVbdpR6ZGaqkNnGVoH5UdzO3P-HlAdu8yj3T197U7Q%3D%3D?uid=0&filename=warn.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=0&tknv=v2&size=2048x2048"
        alt="Предупреждение"
      </a>';} ?>
                Согласие на обработку</br>
                персональных данных</label>
                </div>
            </div>
        </div>

            <input <?php if ($errors['check']) {print 'style="margin-left:-60px"';} ?>
                <?php if($values['check_value']=="1"){print 'checked';}?>id="formcheck" type="checkbox" name="checkbox" value="1">


    <input  style="color:white;" type="submit" id="send" class="buttonform" value="Отправить">
    </div>

</div>
</form>
</div>
</body>
</div>
</html>
